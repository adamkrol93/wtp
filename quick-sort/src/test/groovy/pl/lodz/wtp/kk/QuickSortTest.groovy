package pl.lodz.wtp.kk

import spock.lang.Specification
import spock.lang.Unroll

/**
 * Created by Adam Król on 04.10.2016.
 * @author Adam Król

 */
class QuickSortTest extends Specification {

    @Unroll
    def 'should sort correctly input array'() {
        given:
        QuickSort quickSort = new QuickSort()
        Object[] integers = values;

        when:
        def sort = quickSort.quickSort(integers, new Comparator<Integer>() {
            @Override
            int compare(Integer o1, Integer o2) {
                return o1 - o2;
            }
        })

        then:
        println(sort)
        sort.length == sorted.toArray().length
        sort == sorted.toArray()

        where:
        values                         || sorted
        [7, 5]                         || [5, 7]
        [5, 7]                         || [5, 7]
        [7, 7]                         || [7, 7]
        [7, 7, 7]                      || [7, 7, 7]
        [1, 6, 5, 3, 9]                || [1, 3, 5, 6, 9]
        [1, 3, 5]                      || [1, 3, 5]
        [1, 12, 5, 26, 7, 14, 3, 7, 2] || [1, 2, 3, 5, 7, 7, 12, 14, 26]

    }
}
