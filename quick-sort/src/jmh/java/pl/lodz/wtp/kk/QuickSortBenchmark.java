package pl.lodz.wtp.kk;

import org.apache.commons.math3.distribution.NormalDistribution;
import org.apache.commons.math3.random.RandomGenerator;
import org.apache.commons.math3.random.RandomGeneratorFactory;
import org.openjdk.jmh.annotations.Benchmark;
import org.openjdk.jmh.annotations.BenchmarkMode;
import org.openjdk.jmh.annotations.Level;
import org.openjdk.jmh.annotations.Measurement;
import org.openjdk.jmh.annotations.Mode;
import org.openjdk.jmh.annotations.OutputTimeUnit;
import org.openjdk.jmh.annotations.Scope;
import org.openjdk.jmh.annotations.Setup;
import org.openjdk.jmh.annotations.State;
import org.openjdk.jmh.annotations.Threads;
import org.openjdk.jmh.annotations.Warmup;
import org.openjdk.jmh.runner.Runner;
import org.openjdk.jmh.runner.RunnerException;
import org.openjdk.jmh.runner.options.Options;
import org.openjdk.jmh.runner.options.OptionsBuilder;

import java.util.Arrays;
import java.util.Comparator;
import java.util.Random;
import java.util.concurrent.TimeUnit;

/**
 * Created by Adam Król on 06.10.2016.
 *
 * @author Adam Król
 */
@OutputTimeUnit(TimeUnit.MILLISECONDS)
@BenchmarkMode(Mode.AverageTime)
@Warmup(iterations = 3, time = 1)
@Measurement(iterations = 5, time = 1)
@State(Scope.Thread)
@Threads(4)
public class QuickSortBenchmark {

    private Integer[] input;

    @Setup(Level.Trial)
    public void setup() {
        RandomGenerator randomGenerator = RandomGeneratorFactory.createRandomGenerator(new Random());
        int size = 10000;
        input = new Integer[size];
        for (int i = 0; i < size; i ++) {
            input[i] = randomGenerator.nextInt();
        }
    }

    @Benchmark
    public void quickSortPerformance() {
        QuickSort<Integer> quickSort = new QuickSort<>();
        quickSort.quickSort(input, (o1, o2) -> o1 -o2);
    }

    @Benchmark
    public void javaSortPerformance() {
        Arrays.sort(input);
    }

    public static void main(String[] args) throws RunnerException {
        Options opt = new OptionsBuilder()
                .include("pl.lodz.wtp.kk." + QuickSortBenchmark.class.getSimpleName() + ".*")
                .forks(8)
                .build();

        new Runner(opt).run();
    }

}
