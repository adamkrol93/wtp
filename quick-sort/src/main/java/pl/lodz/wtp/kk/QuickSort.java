package pl.lodz.wtp.kk;

import org.apache.commons.lang3.ArrayUtils;

import java.util.Comparator;

/**
 * Created by Adam Król on 04.10.2016.
 *
 * @author Adam Król
 */
public class QuickSort<T> {

    public T[] quickSort(T[] inputArray, Comparator<T> comparator) {
        T middle = inputArray[(inputArray.length - 1) / 2];
        int leftIndex = 0;
        int rightIndex = inputArray.length - 1;
        T[] clonedArray = inputArray.clone();

        while (leftIndex <= rightIndex) {
            while (comparator.compare(clonedArray[leftIndex], middle) < 0) {
                leftIndex++;
            }
            while (comparator.compare(clonedArray[rightIndex], middle) > 0) {
                rightIndex--;
            }

            if (leftIndex <= rightIndex) {
                T temp = clonedArray[leftIndex];
                clonedArray[leftIndex] = clonedArray[rightIndex];
                clonedArray[rightIndex] = temp;
                rightIndex--;
                leftIndex++;
            }
        }

        T[] leftArray = ArrayUtils.subarray(clonedArray, 0, leftIndex);
        if (0 < leftIndex - 1) {
            leftArray = quickSort(leftArray, comparator);
        }

        T[] rightArray = ArrayUtils.subarray(clonedArray, leftIndex, clonedArray.length);
        if (leftIndex < inputArray.length - 1) {
            rightArray = quickSort(rightArray, comparator);
        }

        return ArrayUtils.addAll(leftArray, rightArray);

    }
}
