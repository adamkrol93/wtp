package pl.lodz.wtp.kk.opencl;

import org.jocl.CL;
import org.jocl.Pointer;
import org.jocl.Sizeof;
import org.jocl.cl_command_queue;
import org.jocl.cl_context;
import org.jocl.cl_context_properties;
import org.jocl.cl_device_id;
import org.jocl.cl_kernel;
import org.jocl.cl_mem;
import org.jocl.cl_platform_id;
import org.jocl.cl_program;

import java.time.Instant;
import java.util.Random;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.Supplier;

import static org.jocl.CL.*;

/**
 * Created by Adam Król on 02.11.2016.
 *
 * @author Adam Król
 */
public class MatrixMultiplier {

    public static void iterate(Consumer<Integer> consumer, int size) {
        for (int i = 0; i < size ; i++) {
            consumer.accept(i);
        }
    }

    public static void main(String args[]) {
        int n = 1000;
        int multiplierSize = 10;
        float srcArrayA[] = new float[n * n];
        float srcArrayB[] = new float[n * multiplierSize];
        float dstArray[] = new float[n * multiplierSize];
        final Random r = new Random();

        Consumer<Consumer<Integer>> overNIterate = consumer -> iterate(consumer, n);

        Consumer<Consumer<Integer>> overMultiplierIterate = consumer -> iterate(consumer, multiplierSize);

        overNIterate.accept( (i) -> {
            overNIterate.accept( (j) -> srcArrayA[i * n + j] = r.nextFloat());
            overMultiplierIterate.accept( (j) -> srcArrayB[i * multiplierSize + j] = r.nextFloat());
        });

        Pointer srcA = Pointer.to(srcArrayA);
        Pointer srcB = Pointer.to(srcArrayB);
        Pointer dst = Pointer.to(dstArray);

        // The platform, device type and device number
        // that will be used
        final int platformIndex = 0;
        final long deviceType = CL_DEVICE_TYPE_GPU;
        final int deviceIndex = 0;

        // Enable exceptions and subsequently omit error checks in this sample
        CL.setExceptionsEnabled(true);

        // Obtain the number of platforms
        int numPlatformsArray[] = new int[1];
        clGetPlatformIDs(0, null, numPlatformsArray);
        int numPlatforms = numPlatformsArray[0];

        // Obtain a platform ID
        cl_platform_id platforms[] = new cl_platform_id[numPlatforms];
        clGetPlatformIDs(platforms.length, platforms, null);
        cl_platform_id platform = platforms[platformIndex];

        // Initialize the context properties
        cl_context_properties contextProperties = new cl_context_properties();
        contextProperties.addProperty(CL_CONTEXT_PLATFORM, platform);

        // Obtain the number of devices for the platform
        int numDevicesArray[] = new int[1];
        clGetDeviceIDs(platform, deviceType, 0, null, numDevicesArray);
        int numDevices = numDevicesArray[0];

        // Obtain a device ID
        cl_device_id devices[] = new cl_device_id[numDevices];
        clGetDeviceIDs(platform, deviceType, numDevices, devices, null);
        cl_device_id device = devices[deviceIndex];

        // Create a context for the selected device
        cl_context context = clCreateContext(
                contextProperties, 1, new cl_device_id[]{device},
                null, null, null);

        // Create a command-queue for the selected device
        cl_command_queue commandQueue =
                clCreateCommandQueue(context, device, 0, null);

        // Allocate the memory objects for the input- and output data
        cl_mem memObjects[] = new cl_mem[5];
        memObjects[0] = clCreateBuffer(context,
                CL_MEM_READ_ONLY | CL_MEM_COPY_HOST_PTR,
                Sizeof.cl_float * n * n, srcA, null);
        memObjects[1] = clCreateBuffer(context,
                CL_MEM_READ_ONLY | CL_MEM_COPY_HOST_PTR,
                Sizeof.cl_float * n * multiplierSize, srcB, null);
        memObjects[2] = clCreateBuffer(context,
                CL_MEM_READ_WRITE,
                Sizeof.cl_float * n * multiplierSize, null, null);
        memObjects[3] = clCreateBuffer(context,
                CL_MEM_READ_ONLY | CL_MEM_COPY_HOST_PTR,
                Sizeof.cl_float, Pointer.to(new int[]{n}), null);
        memObjects[4] = clCreateBuffer(context,
                CL_MEM_READ_ONLY | CL_MEM_COPY_HOST_PTR,
                Sizeof.cl_float, Pointer.to(new int[]{multiplierSize}), null);

        // Create the program from the source code
        String programSource = "__kernel void " +
                "matrixMultiplier(__global const float *a," +
                "             __global const float *b," +
                "             __global float *c, " +
                "             __global int *size," +
                "             __global int *multiplierSize)" +
                "{" +
                "    int column = get_global_id(0);" +
                "    int row = get_global_id(1);" +
                "    float value = 0;" +
                "    for (int k = 0; k < size[0]; ++k) {" +
                "     float elementA = a[row * size[0] + k];" +
                "     float elementB = b[row * multiplierSize[0] + column];" +
                "     value += elementA * elementB;" +
                "    }" +
                "    c[row * multiplierSize[0] + column] = value;" +
                "}";
        cl_program program = clCreateProgramWithSource(context,
                1, new String[]{programSource}, null, null);

        // Build the program
        clBuildProgram(program, 0, null, null, null, null);

        // Create the kernel
        cl_kernel kernel = clCreateKernel(program, "matrixMultiplier", null);

        // Set the arguments for the kernel
        for (int i = 0 ; i < memObjects.length; i++) {
            clSetKernelArg(kernel, i, Sizeof.cl_mem, Pointer.to(memObjects[i]));
        }

        // Set the work-item dimensions
        long global_work_size[] = new long[]{n, n};
        long local_work_size[] = new long[]{1, 1};

        long t1Start = Instant.now().toEpochMilli();
        // Execute the kernel
        clEnqueueNDRangeKernel(commandQueue, kernel, 2, null,
                global_work_size, local_work_size, 0, null, null);
        long t1End = Instant.now().toEpochMilli();

        // Read the output data
        clEnqueueReadBuffer(commandQueue, memObjects[2], CL_TRUE, 0,
                n * multiplierSize * Sizeof.cl_float, dst, 0, null, null);

        clearMemory(context, commandQueue, memObjects, program, kernel);

        // Verify the result
        boolean passed = true;
        final float epsilon = 1e-7f;
        float[] dstArraySecond = new float[n * multiplierSize];
        long t2Start = Instant.now().toEpochMilli();

        overNIterate.accept( i -> overMultiplierIterate.accept(j -> {
            final float[] value = {0};
            overNIterate.accept( k -> {
                float elementA = srcArrayA[i * n + k];
                float elementB = srcArrayB[i * multiplierSize + j];
                value[0] += elementA*elementB;
            });
            dstArraySecond[i * multiplierSize + j] = value[0];
        }));

        long t2End = Instant.now().toEpochMilli();


        for (int i = 0; i < n; i++) {
            for (int j = 0; j < multiplierSize; j++) {
                float x = dstArray[i * multiplierSize + j];
                float y = dstArraySecond[i * multiplierSize + j];
                boolean epsilonEqual = Math.abs(x - y) <= epsilon * Math.abs(x);
                if (!epsilonEqual) {
                    passed = false;
                    break;
                }
            }
        }
        System.out.println("Test " + (passed ? "PASSED" : "FAILED"));
//        System.out.println("A : " + Arrays.toString(srcArrayA));
//        System.out.println("B : " + Arrays.toString(srcArrayB));
//        System.out.println("Result: " + java.util.Arrays.toString(dstArray));
        System.out.println("T1 : " + (t1End - t1Start) + " T2 : " + (t2End - t2Start));
    }

    private static void clearMemory(cl_context context, cl_command_queue commandQueue, cl_mem[] memObjects, cl_program program, cl_kernel kernel) {
        for (cl_mem memObject : memObjects) {
            clReleaseMemObject(memObject);
        }

        clReleaseKernel(kernel);
        clReleaseProgram(program);
        clReleaseCommandQueue(commandQueue);
        clReleaseContext(context);
    }
}
